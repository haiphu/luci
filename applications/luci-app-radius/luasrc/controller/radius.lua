module("luci.controller.radius", package.seeall)

function index()
        local page

	page = entry({"admin", "network", "radius"}, cbi("radius/users"), _("Radius"), 60)
	page.dependent = true
end
