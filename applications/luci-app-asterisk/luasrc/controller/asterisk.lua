-- Copyright 2008 Yanira <forum-2008@email.de>
-- Licensed to the public under the Apache License 2.0.

module("luci.controller.asterisk", package.seeall)

function index()
	local page

	page = entry({"admin", "services", "asterisk"}, cbi("asterisk"), _("VoIP"), 60)
	page.dependent = true
end

