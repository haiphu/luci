
-- Copyright Duyho <duyhungho.work@hcmut.edu.vn>
-- Licensed to the public under the Apache License 2.0.

local fs = require("nixio.fs")
local fs = require("nixio.fs")

local knownParams = {
	--
	--  Widget
	--	Name
	--	Default(s)
	--	Description
	--	Option(s)

	{ "Site-to-site", {
	-- initialisation and daemon options
		{ ListValue,
			"authentication_method",
			{ "pubkey","psk","eap-mschapv2","xauth","xauth-generic" },
			translate("Set output authentication")},
		{ Value,
			"pre_shared_key",
			0,
			translate("Set up Pre-shared key if using psk") },
		{ Value,
			"keyingtries",
			0,
			translate("Time for keyingtries") },
		{ Value,
			"ikelifetime",
			0,
			translate("Set Ike life time") },
		{ Value,
			"dpddelay",
			0,
            translate("set dead pear detection") },
        { Value,
			"dpdtimeout",
			0,
            translate("set time of dead pear detection") },
        { ListValue,
			"dpdaction",
			{ "none","hold","clear","restart" },
            translate("set action for dead pear detection") },
        { ListValue,
			"phase1_proposal",
            {"aes192-sha256-modp1024"},
			translate("set cipher suite/ refer: strongswan cipher suite") },
		{ Value,
			"remotegateway",
			"0.0.0.0",
			translate("host name or ip address") },
		{ Value,
			"remote_subnet",
			"0.0.0.0/24",
			translate("subnet /lan ip address") },
		{ Value,
			"localgateway",
			"0.0.0.0",
			translate("host name or ip address") },
		{ Value,
			"local_subnet",
			"0.0.0.0/24",
			translate("subnet of Lan") },
		{ Value,
			"lifetime",
			0,
            translate("set up life time of esp tunnel") },
        { ListValue,
			"phase2_proposal",
            {"aes256gcm128-sha256-modp1024"},
            translate("set cipher suite/ refer: strongswan cipher suite") },
        } },

-- 	{ "IPSec", {
-- 	-- socket config
-- 		{ Value,
-- 			"remotegateway",
-- 			"0.0.0.0",
-- 			translate("host name or ip address") },
-- 		{ Value,
-- 			"remote_subnet",
-- 			"0.0.0.0/24",
-- 			translate("subnet /lan ip address") },
-- 		{ Value,
-- 			"localgateway",
-- 			"0.0.0.0",
-- 			translate("host name or ip address") },
-- 		{ Value,
-- 			"local_subnet",
-- 			"0.0.0.0/24",
-- 			translate("subnet of Lan") },
-- 		{ Value,
-- 			"lifetime",
-- 			0,
--             translate("set up life time of esp tunnel") },
--         { ListValue,
-- 			"phase2_proposal",
--             {"aes256gcm128-sha256-modp1024"},
--             translate("set cipher suite/ refer: strongswan cipher suite") },
-- 	} }
-- }

local cts = { }
local params = { }

local m = Map("ipsec")
m.redirect = luci.dispatcher.build_url("admin", "vpn", "ipsecvpn")
m.apply_on_parse = true

local p = m:section( SimpleSection )
p.template = "ipsec-module/pageswitch"
p.mode     = "basic"
p.instance = arg[1]
p.category = arg[2] or "IKE"

for _, c in ipairs(knownParams) do
	cts[#cts+1] = c[1]
	if c[1] == p.category then params = c[2] end
end

p.categories = cts


local s = m:section(
	NamedSection, arg[1], "remote",
	translate("%s" % arg[2])
)

s.title     = translate("%s" % arg[2])
s.addremove = false
s.anonymous = true


for _, option in ipairs(params) do
	local o = s:option(
		option[1], option[2],
		option[2], option[4]
	)

	o.optional = true

	if option[1] == DummyValue then
		o.value = option[3]
	elseif option[1] == FileUpload then

		function o.cfgvalue(self, section)
			local cfg_val = AbstractValue.cfgvalue(self, section)

			if cfg_val then
				return cfg_val
			end
		end

		function o.formvalue(self, section)
			local sel_val = AbstractValue.formvalue(self, section)
			local txt_val = luci.http.formvalue("cbid."..self.map.config.."."..section.."."..self.option..".textbox")

			if sel_val and sel_val ~= "" then
				return sel_val
			end

			if txt_val and txt_val ~= "" then
				return txt_val
			end
		end

		function o.remove(self, section)
			local cfg_val = AbstractValue.cfgvalue(self, section)
			local txt_val = luci.http.formvalue("cbid."..self.map.config.."."..section.."."..self.option..".textbox")
			
			if cfg_val and fs.access(cfg_val) and txt_val == "" then
				fs.unlink(cfg_val)
			end
			return AbstractValue.remove(self, section)
		end
	elseif option[1] == Flag then
		o.default = nil
	else
		if option[1] == DynamicList then
			function o.cfgvalue(...)
				local val = AbstractValue.cfgvalue(...)
				return ( val and type(val) ~= "table" ) and { val } or val
			end
		end

		if type(option[3]) == "table" then
			if o.optional then o:value("", "-- remove --") end
			for _, v in ipairs(option[3]) do
				v = tostring(v)
				o:value(v)
			end
			o.default = tostring(option[3][1])
		else
			o.default = tostring(option[3])
		end
	end

	for i=5,#option do
		if type(option[i]) == "table" then
			o:depends(option[i])
		end
	end
end

return m
